![screenshot](https://steamuserimages-a.akamaihd.net/ugc/945077538412342299/6FA531F13D0BF8AFD992908DCFFE7EFD8AB5244D/)

# Howto
This is suited for [barebones](https://gitlab.com/alaah/dead-cells-barebones).

# License
See LICENSE for information.

Of course this is based on absolutely proprietary files. I consider my "changes" to be GPL. No one will care anyway.